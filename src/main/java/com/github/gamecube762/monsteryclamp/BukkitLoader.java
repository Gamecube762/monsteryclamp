package com.github.gamecube762.monsteryclamp;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class BukkitLoader extends JavaPlugin implements Listener {



    FileConfiguration config;

    @Override
    public void onLoad() {
        getDataFolder().mkdirs();
        config = getConfig();
    }

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        getCommand("monsterclampreload").setExecutor(this);
        getServer().getWorlds().forEach(this::addWorldToConfigDefaults);
        saveConfig();
    }

    @EventHandler
    public void onWorldLoad(WorldLoadEvent event) {
        addWorldToConfigDefaults(event.getWorld());
        saveConfig();
    }

    private void addWorldToConfigDefaults(World world) {
        String uuid = world.getUID().toString();
        config.addDefault(uuid + ".yup", 20);
        config.addDefault(uuid + ".ydn", 20);
        config.options().copyDefaults();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        reloadConfig();
        return true;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onMonsterSpawn(EntitySpawnEvent event) {
        if (!(event.getEntity() instanceof Monster) || event.getEntityType() != EntityType.BAT || event.getEntityType() != EntityType.SLIME)
            return;

        String uuid = event.getLocation().getWorld().getUID().toString();
        double dist = distanceToClosestPlayer(event.getLocation());

        if (Math.abs(dist) > (dist > 0 ? config.getInt(uuid + "yup", 20) : config.getInt(uuid + "ydn", 20)))
            event.setCancelled(true);

    }

    public double distanceToClosestPlayer(Location location) {
        return Bukkit.getOnlinePlayers()
                .stream()
                .filter(p -> p.getLocation().getWorld() == location.getWorld())
                .map(p -> location.getY() - p.getLocation().getY())
                .reduce(0D, Math::min);
    }
}
